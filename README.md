#Сборщик мусора

##Задание:

Реализовать приложение Django, позволяющее просматривать все файлы, загруженные пользователями (дирректория media), и удалять файлы, оставшиеся без привязки к какой либо модели в текущем проекте.
Интерфейс реализовать в административной панели Django (с фильтрами, поиском и действиями). Предусмотреть создание новой модели для хранения информации о файлах (количество и типы полей на усмотрение соискателя).

##Условия:

1. __Python 3.6+__
2. __Django 2.2+__
3. Можно использовать любые общедоступные библиотеки, которые сочтёте нужным;
4. Чем меньше кода, тем лучше. __PEP8__ — обязательно;
5. Если в условиях вам не хватает каких-то данных, опирайтесь на здравый смысл.

##Если задачу удалось сделать быстро, и у вас еще остался энтузиазм, то будет большим плюсом:

1. Покрыть код тестами (`unittest` или `pytest`)
2. Реализовать в виде сторонней библиотеки (установка через `pip install`)
3. Реализовать API и написать front на vue:
    * таблица со списком всех файлов в директории media (в отдельных столбцах полный путь и модель)
    * возможность выбирать файлы в таблице и удалять их (запись в БД и сам файл)
    * возможность фильтровать файлы по признаку наличия привязки к модели
4. Упаковать все в Docker контейнер (в случае невыполнения пункта №2).

##Критерии оценки:

1. Скорость выполнение задания
2. Форматирование кода и наличие комментариев
3. Алгоритм реализации
4. Творческий подход (например, использование `django-grappelli`)
5. Адекватность восприятия задания и отсутствие лишних вопросов ;)


#Присылайте ваше решение в виде ссылки на публичный репозиторий на Github или BitBucket.